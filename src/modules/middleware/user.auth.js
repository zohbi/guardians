module.exports = {
  ensureAuthenticated: (req, res, next) => {
    
    if (
      req.session.application == null ||
      Object.keys(req.session.application).length == 0
    ) {
      return res.redirect('/login');
    } else {
      if (req.session.application.emailVerify.status === false) {
        return res.redirect('/email-verify');
      }
      next();
    }
  },

  forwardAuthenticated: (req, res, next) => {
    
    if (req.session.application != null) {
      if (req.session.application.isApprove) {
        return res.redirect('/accepted-form');
      } else if (req.session.application.emailVerify.status == false) {
        // return res.redirect('/email-verify');
      } else {
        return res.redirect('/register-detail');
      }
    }
    next();
  },
};
