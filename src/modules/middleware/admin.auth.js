module.exports = {
  ensureAuthenticated: (req, res, next) => {
    if (
      req.session.admin == null ||
      Object.keys(req.session.admin).length == 0
    ) {
      res.redirect('/admin/login'); // if not auth
    }
    return next();
  },

  forwardAuthenticated: (req, res, next) => {
    if (req.session.admin == null) {
      return next();
    }
    res.redirect('/admin/dashboard'); // if auth
  },
};
