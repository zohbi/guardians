const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const SALT_FACTOR = 10;

var userSchema = mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    registrationType: { type: String, required: true },
    productTrade: { type: String, required: true },
    hearAbout: { type: String, required: true },

    //1

    firstName: { type: String, default: null },
    lastName: { type: String, default: null },
    initial: { type: String, default: null },
    address: { type: String, default: null },
    aptSuite: { type: String, default: null },
    country: { type: String, default: null },
    province: { type: String, default: null },
    city: { type: String, default: null },
    zipCode: { type: String, default: null },
    mailAddother: { type: Boolean, default: false },
    mailingAddress: { type: String, default: null },
    mailingAptsuite: { type: String, default: null },
    mailingCountry: { type: String, default: null },
    mailingProvince: { type: String, default: null },
    mailingCity: { type: String, default: null },
    mailingZipcode: { type: String, default: null },
    teleNum: { type: String, default: null },
    martialStatus: { type: String, default: null },
    numberOfDependent: { type: Number, default: null },
    trustedPerson: { type: Boolean, default: false },
    trustedPersonName: { type: String, default: null },
    trustedPersonPhone: { type: String, default: null },
    trustedPersonEmail: { type: String, default: null },
    trustedPersonStreet: { type: String, default: null },
    trustedPersonCountry: { type: String, default: null },
    trustedPersonState: { type: String, default: null },
    trustedPersonCity: { type: String, default: null },
    trustedPersonZip: { type: String, default: null },
    trustedPersonRelation: { type: String, default: null },
    trusteddob: { type: Date, default: null },
    //2
    employee: { type: String, default: null },
    employerName: { type: String, default: null },
    employerOccupation: { type: String, default: null },
    employerAddress: { type: String, default: null },
    employerYearWithEmployer: { type: String, default: null },
    employerCountry: { type: String, default: null },
    employerState: { type: String, default: null },
    employerCity: { type: String, default: null },
    employerAptsuite: { type: String, default: null },
    employerPhoneNumber: { type: String, default: null },
    employerFax: { type: String, default: null },

    //3
    taxpayerIDNo: { type: String, default: null },
    dateOfBirth: { type: Date, default: null },
    idType: { type: String, default: null },
    nameOfId: { type: String, default: null },
    idNumber: { type: String, default: null },
    expDate: { type: Date, default: null },
    placeCountry: { type: String, default: null },
    placeState: { type: String, default: null },
    issueDate: { type: Date, default: null },
    taxResidance: { type: String, default: null },
    taxResidanceState: { type: String, default: null },
    annualIncome: { type: String, default: null },
    netWorth: { type: String, default: null },
    liquidNetWorth: { type: String, default: null },
    taxRate: { type: String, default: null },
    incomeAccount: { type: Boolean, default: false },
    retireSaving: { type: Boolean, default: false },
    gift: { type: Boolean, default: false },
    businessProp: { type: Boolean, default: false },
    inheritence: { type: Boolean, default: false },
    secuBnefit: { type: Boolean, default: false },
    otherCheckbox: { type: Boolean, default: false },
    otherInput: { type: String, default: null },
    bankFunding: { type: String, default: null },
    abaSwift: { type: String, default: null },
    accountNumber: { type: String, default: null },
    accountRisk: { type: String, default: null },
    conservative: { type: Boolean, default: false },
    moderatelyConservative: { type: Boolean, default: false },
    moderate: { type: Boolean, default: false },
    moderatelyAggressive: { type: Boolean, default: false },
    significantRisk: { type: Boolean, default: false },

    annualExpense: { type: String, default: null },
    specialExpense: { type: String, default: null },
    liquidityExpense: { type: String, default: null },
    financialGoal: { type: String, default: null },
    investStock: { type: Boolean, default: null },
    stockExp: { type: String, default: null },
    stockExpertise: { type: String, default: null },
    investBond: { type: Boolean, default: null },
    bondExp: { type: String, default: null },
    bondExpertise: { type: String, default: null },
    investOptions: { type: Boolean, default: null },
    optionsExp: { type: String, default: null },
    optionsExpertise: { type: String, default: null },
    investFuture: { type: Boolean, default: null },
    futureExp: { type: String, default: null },
    futureExpertise: { type: String, default: null },
    brokerAccount: { type: String, default: null },
    brokerAccountTotal: { type: String, default: null },
    beneficial: { type: Boolean, default: null },
    beneficialAccountNumber: { type: String, default: null },
    beneficialAccountName: { type: String, default: null },
    shareHolder: { type: Boolean, default: null },
    shareHolderAccountNumber: { type: String, default: null },
    shareHolderAccountName: { type: String, default: null },
    shareHolderRelationship: { type: String, default: null },
    immediate: { type: Boolean, default: null },
    immediateCompanyName: { type: String, default: null },
    immediateCompanyaddress: { type: String, default: null },
    immediateRelationshipwithentity: { type: String, default: null },
    securities: { type: Boolean, default: null },
    securitiesFirm: { type: String, default: null },
    securitiesFirmAddress: { type: String, default: null },
    securitiesPermissionAccount: { type: String, default: null },

    institution: { type: Boolean, default: null },
    institutionFirm: { type: String, default: null },
    institutionFirmAddress: { type: String, default: null },
    institutionPositioninFirm: { type: String, default: null },
    securitiesPermissionAccount: { type: String, default: null },
    taxWitholding: { type: String, default: null },
    underUs: { type: String, default: null },
    underUs2: { type: String, default: null },

    // senior: { type: Boolean, default: null },
    // seniorField: { type: String, default: null },
    // houseHold: { type: Boolean, default: null },
    // compnyName: { type: String, default: null },
    // symbol: { type: String, default: null },
    // municipal: { type: Boolean, default: null },
    // affiName: { type: String, default: null },
    // affiEntityName: { type: String, default: null },
    // affiAdre: { type: String, default: null },
    // affiCountry: { type: String, default: null },
    // affiCity: { type: String, default: null },
    // affiState: { type: String, default: null },
    // affiZip: { type: String, default: null },
    optRadio: { type: Boolean, default: false },
    borrowInput: { type: String, default: null },
    accountTerms: { type: Boolean, default: false },
    riskDis: { type: Boolean, default: false },
    pennyStocks: { type: Boolean, default: false },
    electronicAccess: { type: Boolean, default: false },
    marginDisclosure: { type: Boolean, default: false },
    w9_Certification: { type: Boolean, default: false },
    stock_Locate: { type: Boolean, default: false },
    marginDisc: { type: Boolean, default: false },
    confirmedElectronic: { type: Boolean, default: null },

    photo: { type: [String], default: null },

    signatureName: { type: String, default: null },

    //Joint Signature
    jointEmail: { type: String, default: null },
    jointSignatureName: { type: String, default: null },
    isJoint: { type: Boolean, default: null },
    isJointDraft: { type: Boolean, default: null },

    isApprove: { type: Boolean, default: null },
    isDraft: { type: Boolean, default: true },
    isForgot: { type: Boolean, default: false },
    forgotToken: { type: String, default: null },
    emailVerify: {
      status: { type: Boolean, default: false },
      verification_code: { type: String, default: null },
    },
    // Corporate Fields
    companyName               : {type: String, default: null},
    companyAddress            : {type: String, default: null},
    companyAptSuite           : {type: String, default: null},
    companyCountry            : {type: String, default: null},
    companyStateProvince      : {type: String, default: null},
    companyCity               : {type: String, default: null},
    companyZipCode            : {type: String, default: null},
    companyEmail              : {type: String, default: null},
    companyPhoneNo            : {type: String, default: null},
    corporate_photo           : {type: [String], default: null },
    companyMailAddress        : {type: String, default: null},
    companyMailAptSuite       : {type: String, default: null},
    companyMailCountry        : {type: String, default: null},
    companyMailStateProvince  : {type: String, default: null},
    companyMailCity           : {type: String, default: null},
    companyMailZipCode        : {type: String, default: null},

    uploadSection  :{type: String, default: "1-Government Issued ID"},
    adminSignature :{type: String, default: "1-Government Issued ID"},

    userIdentificationCode    : {type: String, default: null},
    
    authorizedIndividual: { type: Boolean, default: null },
    authorizedIndividualRelation: { type: String, default: null },
    authorizedIndividualPosition: { type: String, default: null },
    authorizedIndividualName: { type: String, default: null },
  },
  { timestamps: true }
);

userSchema.pre('save', function (done) {
  var user = this;

  if (!user.isModified('password')) {
    return done();
  }

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) {
      return done(err);
    }
    bcrypt.hash(user.password, salt, function (err, hashedPassword) {
      if (err) {
        return done(err);
      }
      user.password = hashedPassword;
      done();
    });
  });
});

userSchema.pre('findOneAndUpdate', function (done) {
  var user = this;
  if (!user._update.password) {
    return done();
  }

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) {
      return done(err);
    }
    bcrypt.hash(user._update.password, salt, function (err, hashedPassword) {
      if (err) {
        return done(err);
      }
      user._update.password = hashedPassword;
      done();
    });
  });
});

var User = mongoose.model('User', userSchema);

module.exports = User;
