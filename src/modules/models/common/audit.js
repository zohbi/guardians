const mongoose = require('mongoose');

const auditSchema = new mongoose.Schema({
  userId        : { type: mongoose.Schema.Types.ObjectId, ref: 'User',  default: null },
  rejectUserId  : { type: mongoose.Schema.Types.ObjectId, ref: 'rejectedUser',  default: null },
  signupForm: {
    type:{ type: String, enum: ['Signup', null], default: null },
    location:{ type: String, default:null }, 
    ipaddress:{ type: String, default:null },
    date: { type: Date, default: null }, 
  },
  formSubmitted: {
    type:{ type: String, enum: ['Form Submisson',null], default: null },
    date: { type: Date, default: null },
  },
  applicationStatus: {
    type:{ type: String, enum: ['Application accepted','Application rejected',null], default: null },
    admin: { type: mongoose.Schema.Types.ObjectId, ref: 'Admin',  default: null },
    message:{ type: String, default:null },
    reason:{ type: String, default:null },
    date: { type: Date, default: null }, 
  },
  draftEmail: [{
    type:{ type: String, enum: 'Draft Email', default: null },
    emailSend: { type: mongoose.Schema.Types.ObjectId, ref: 'Admin',  default: null },
    message:{ type: String, default:null },
  }],
},{timestamps: true});



module.exports = mongoose.model('Audit', auditSchema);