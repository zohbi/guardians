const Countries = require('../models/common/countries');

exports.getCountries = async (req, res) => {
  const countries = await Countries.find({}).lean();
  return res.json({ countries });
};

exports.getState = async (req, res) => {
  const countries = await Countries.findOne({ name: req.params.id }).lean();
  if (countries) {
    return res.json({ states: countries.states || '' });
  } else {
    return res.json({ states: '' });
  }
};
