const express = require('express');
let pdf = require('html-pdf');
let ejs = require('ejs');
let path = require('path');
const fs = require('fs');
const url = require('url');
const https = require('http');
const pdfshift = require('pdfshift')('c81b6bf0750c42b1bcc25586c03e50af');
const router = express.Router();
const {
  getLogin,
  postLogin,
  checkEmail,
  getAcceptedForm,
  getRegister,
  postRegister,
  getRegisterDetail,
  postRegisterDetail1,
  postRegisterDetail2,
  postRegisterDetail3,
  postRegisterDetail4,
  postRegisterDetail5,
  postRegisterDetail6,
  postRegisterDetail7,
  postRegisterDetail8,
  postRegisterDetail9,
  postRegisterDetail10,
  signatureSave,
  postRegisterDetail11,
  logout,
} = require('./auth.controller');
const {
  getForgotPassword,
  postForgotPassword,
  getError,
  getNewPassword,
  postNewPassword,
} = require('./forgot.controller');
const { getCountries, getState } = require('./countries.controller');
const {
  getEmailVerify,
  postEmailVerify,
  ResendEmail,
} = require('./email-verify.controller');
const multer = require('multer');

const {
  ensureAuthenticated,
  forwardAuthenticated,
} = require('../middleware/user.auth');

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './src/modules/public/users/images');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '_' + Date.now() + '_' + file.originalname);
  },
});

const upload = multer({storage: storage});

router.get('/login', forwardAuthenticated, getLogin);
router.post('/login', forwardAuthenticated, postLogin);
router.get('/accepted-form', ensureAuthenticated, getAcceptedForm);
router.get('/register', getRegister);
router.post('/register', postRegister);
// router.post('/register', ()=>{console.log("qwerty")}); // postRegister
router.post('/email-check', checkEmail);
router.get('/register-detail', ensureAuthenticated, getRegisterDetail);

router.post('/register-detail1', ensureAuthenticated, postRegisterDetail1);
router.post('/register-detail2', ensureAuthenticated, postRegisterDetail2);
router.post('/register-detail3', ensureAuthenticated, postRegisterDetail3);
router.post('/register-detail4', ensureAuthenticated, postRegisterDetail4);
router.post('/register-detail5', ensureAuthenticated, postRegisterDetail5);
router.post('/register-detail6', ensureAuthenticated, postRegisterDetail6);
router.post('/register-detail7', ensureAuthenticated, postRegisterDetail7);
router.post('/register-detail8', ensureAuthenticated, postRegisterDetail8);
router.post('/register-detail9', ensureAuthenticated,
  upload.array('photo[]', 8), postRegisterDetail9
);
router.post(
  '/register-detail10',
  upload.single('securitiesPermissionAccount'),
  ensureAuthenticated,
  postRegisterDetail10
);
router.post('/register-detail11', ensureAuthenticated, postRegisterDetail11);

router.get('/forgot-password', getForgotPassword);
router.post('/forgot-password', postForgotPassword);
router.get('/error', getError);
router.get('/new-password/:id/:token', getNewPassword);
router.post('/new-password/:id', postNewPassword);
router.get('/logout', logout);

router.get('/countries', ensureAuthenticated, getCountries);
router.get('/countries/:id', ensureAuthenticated, getState);
router.get('/email-verify', getEmailVerify);
router.post('/email-verify', postEmailVerify);
router.get('/verification-send', ResendEmail);

router.get('/generate', ensureAuthenticated, (req, res) => {
  res.render('pdf/index', { application: req.session.application });
});

router.post('/signatureSave', ensureAuthenticated, signatureSave);

router.get('/generateReport', (req, res) => {
  // ejs.renderFile(
  //   path.join(__dirname, '../views/pdf', 'index.ejs'),
  //   { application: { asd: 'asd' } },
  //   (err, data) => {
  //     if (err) {
  //       console.log(err);
  //     } else {
  //       pdf
  //         .create(data)
  //         .toFile('src/modules/public/users/pdf/report.pdf', function (
  //           err,
  //           data
  //         ) {
  //           if (err) {
  //             res.send(err);
  //           } else {
  //             console.log('successfully');
  //             res.send('File created successfully' + data);
  //           }
  //         });
  //     }
  //   }
  // );

  res.render(
    'pdf/index',
    { application: req.session.application },
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        pdfshift
          .prepare(data)
          .convert()
          .then(function (binary_file) {
            fs.writeFile('result.pdf', binary_file, 'binary', function () {});
          })
          .catch(function ({ message, code, response, errors }) {
            console.log(response);
          });
      }
    }
  );

  return 'ok';
});

module.exports = router;
