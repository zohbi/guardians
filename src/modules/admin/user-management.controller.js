const Admin = require('../models/admin/admin');

exports.getAdmin = async (req, res) => {
  res.render('admin/user-management');
};

exports.getDatatables = (req, res) => {
  var searchStr = req.body.search.value;
  if (req.body.search.value) {
    var regex = new RegExp(req.body.search.value, 'i');
    searchStr = { email: { $ne: 'admin@gmail.com' }, $or: [{ email: regex }] };
  } else {
    searchStr = { email: { $ne: 'admin@gmail.com' } };
  }

  Admin.countDocuments({}, function (err, c) {
    recordsTotal = c;
    Admin.countDocuments(searchStr, function (err, c) {
      recordsFiltered = c;
      Admin.find(
        searchStr,
        '_id email username role createdAt',
        {
          skip: Number(req.body.start),
          limit: Number(req.body.length),
          sort: '-createdAt',
        },
        function (err, results) {
          if (err) {
            console.log('error while getting results' + err);
            return;
          } else {
            var data = JSON.stringify({
              draw: req.body.draw,
              recordsFiltered: recordsFiltered,
              recordsTotal: recordsTotal,
              data: results,
            });
            return res.send(data);
          }
        }
      );
    });
  });
};

exports.getAddUser = async (req, res) => {
  res.render('admin/add-user');
};

exports.postAddUser = async (req, res) => {
  try {
    const admin = await Admin.findOne({ email: req.body.email }).lean();
    if (!admin) {
      let user = new Admin({
        email: req.body.email,
        password: req.body.password,
        username: req.body.username,
        role: req.body.role,
      });
      await user.save();
      req.flash('success', 'User Created Successfully');
      return res.redirect('/admin/user-management');
    } else {
      res.render('admin/add-user', {
        user_management: {
          email: req.body.email,
          username: req.body.username,
          role: req.body.role,
        },
        error: 'Email Already Exist',
      });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.getEditUser = async (req, res) => {
  try {
    const user_management = await Admin.findOne({ _id: req.params.id }).lean();

    res.render('admin/edit-user', { user_management });
  } catch (error) {
    req.flash('error', 'User Not Found');
    return res.redirect('/admin/user-management');
  }
};

exports.postEditUser = async (req, res) => {
  try {
    let user_management = {
      username: req.body.username,
      password: req.body.password,
      role: req.body.role,
    };
    if (req.body.password == '') {
      user_management = {
        username: req.body.username,
        role: req.body.role,
      };
    }

    let user = await Admin.findByIdAndUpdate(req.params.id, user_management, {
      new: true,
    });
    req.flash('success', 'User Updated Successfully');
    return res.redirect('/admin/user-management');
  } catch (err) {
    console.log(err);
  }
};

exports.postDeleteUser = async (req, res) => {
  try {
    const user_management = await Admin.findByIdAndDelete(req.params.id);
    req.flash('success', 'User Deleted Successfully');
    return res.redirect('/admin/user-management');
  } catch (err) {
    console.log(err);
  }
};
