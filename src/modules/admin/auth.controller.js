const Admin = require('../models/admin/admin');
const User = require('../models/user/user');
const rejectedUser = require('../models/user/rejectedUser');
const passport = require('passport');
const Configuration = require('../models/admin/configuration');
const bcrypt = require('bcryptjs');
const fs = require('fs');


exports.adminSignatureSave = async (req, res) => {
	try {
		const img = req.body.dataURL;
		const data = img.replace(/^data:image\/\w+;base64,/, '');
		const buf = Buffer.from(data, 'base64');
		const filename = req.body.adminId + Date.now() + '.png'
		const signatureName = 'src/modules/public/admin/signatures/' + filename;
		fs.writeFile(signatureName, buf, function (err) {
			if (err) {
				res.json({
					success: 0
				});
			}
		});
		const updateUser = await Admin.findByIdAndUpdate(
			req.body.adminId, {
				signatureName: '/admin/signatures/' + filename,
			}, {
				new: true,
			}
		);
		req.session.admin = updateUser;
		res.json({
			success: 1,
			img: filename
		});

	} catch (err) {
		console.log(err);
	}
};
exports.getLogin = (req, res) => {
	res.render('admin/login');
};

exports.postLogin = async (req, res, next) => {
	const admin = await Admin.findOne({
		email: req.body.email
	});

	if (!admin) {
		req.flash('error', 'That Email is Not Registered');
		return res.redirect('/admin/login');
	}
	if (admin) {
		const passwordCompare = await bcrypt.compare(
			req.body.password,
			admin.password
		);

		if (!passwordCompare) {
			req.flash('error', 'Password Incorrect');
			return res.redirect('/admin/login');
		}

		req.session.admin = admin;
	}
	res.redirect('/admin/dashboard');
};

exports.getDasboard = async (req, res) => {
	// const temp_user = await User.findByIdAndUpdate(
	//   "607454cded37e823144f6da9", {
	//     isApprove: null,
	//   }
	// );
	const applicationDraftCount = await User.find({
		isApprove: null,
		isDraft: true,
	}).countDocuments();
	const applicationAcceptCount = await User.find({
		isApprove: true,
		isDraft: false,
	}).countDocuments();
	const applicationRejectCount = await rejectedUser.find({}).countDocuments();
	const applicationPendingCount = await User.find({
		isApprove: null,
		isDraft: false,
	}).countDocuments();

	const applications = await User.find({})
		.sort('-createdAt')
		// .limit(5)
		.lean();

	const applicationDrafts = await User.find({
			isApprove: null,
			isDraft: true
		})
		.sort('-createdAt')
		// .limit(5)
		.lean();

	const applicationPendings = await User.find({
			isApprove: null,
			isDraft: false,
		})
		.sort('-createdAt')
		// .limit(5)
		.lean();

	const configuration = await Configuration.findOne().lean();

	res.render('admin/dashboard', {
		applicationDraftCount,
		applicationAcceptCount,
		applicationRejectCount,
		applicationPendingCount,
		applications,
		applicationDrafts,
		applicationPendings,
		configuration,
	});
};

exports.logout = async (req, res) => {
	req.session.destroy();
	res.redirect('/admin/login');
};
