const User = require('../models/user/user');
const RejectedUser = require('../models/user/rejectedUser');
const Configuration = require('../models/admin/configuration');

exports.getPendingApplication = async (req, res) => {
	const configuration = await Configuration.findOne().lean();
	User.find({
		isApprove: null,
		isDraft: false,
	}, function (err, users) {
		var pendingUsers = JSON.stringify(users);
		res.render('admin/pending-application', {
			pendingUsers,
			configuration
		});
	});
};

exports.postPendingApplication = (req, res) => {
	var searchStr = req.body.search.value;
	if (req.body.search.value) {
		var regex = new RegExp(req.body.search.value, 'i');
		searchStr = {
			isApprove: null,
			isDraft: false,
			$or: [{
				email: regex
			}, {
				firstName: regex
			}, {
				lastName: regex
			}],
		};
	} else {
		searchStr = {
			isApprove: null,
			isDraft: false
		};
	}

	User.countDocuments({
		isApprove: null,
		isDraft: false
	}, function (err, c) {
		recordsTotal = c;
		User.countDocuments(searchStr, function (err, c) {
			recordsFiltered = c;
			User.find(
				searchStr,
				'_id firstName lastName email createdAt', {
					skip: Number(req.body.start),
					limit: Number(req.body.length),
					sort: '-createdAt',
				},
				function (err, results) {
					if (err) {
						console.log('error while getting results' + err);
						return;
					} else {
						var data = JSON.stringify({
							draw: req.body.draw,
							recordsFiltered: recordsFiltered,
							recordsTotal: recordsTotal,
							data: results,
						});
						return res.send(data);
					}
				}
			);
		});
	});
};

exports.getAcceptedApplication = async (req, res) => {
	User.find({
		isApprove	: true,
		isDraft		: false
	}, function (err, users) {
		var acceptedUsers = JSON.stringify(users);
		res.render('admin/accepted-application', {
			acceptedUsers
		});
	});
};

exports.postAcceptedApplication = (req, res) => {
	var searchStr = req.body.search.value;
	if (req.body.search.value) {
		var regex = new RegExp(req.body.search.value, 'i');
		searchStr = {
			isApprove: true,
			isDraft: false,
			$or: [{
				email: regex
			}, {
				firstName: regex
			}, {
				lastName: regex
			}],
		};
	} else {
		searchStr = {
			isApprove: true,
			isDraft: false
		};
	}

	User.countDocuments({
		isApprove: true,
		isDraft: false
	}, function (err, c) {
		recordsTotal = c;
		User.countDocuments(searchStr, function (err, c) {
			recordsFiltered = c;
			User.find(
				searchStr,
				'_id firstName lastName email createdAt', {
					skip: Number(req.body.start),
					limit: Number(req.body.length),
					sort: '-createdAt',
				},
				function (err, results) {
					if (err) {
						console.log('error while getting results' + err);
						return;
					} else {
						var data = JSON.stringify({
							draw: req.body.draw,
							recordsFiltered: recordsFiltered,
							recordsTotal: recordsTotal,
							data: results,
						});
						return res.send(data);
					}
				}
			);
		});
	});
};

exports.getRejectedApplication = async (req, res) => {
	var userMap = {};
	RejectedUser.find({}, function (err, users) {
		var users = JSON.stringify(users);
		res.render('admin/rejected-application', {
			"users": users
		});
	});
};

exports.postRejectedApplication = (req, res) => {
	var searchStr = req.body.search.value;
	if (req.body.search.value) {
		var regex = new RegExp(req.body.search.value, 'i');
		searchStr = {
			isApprove: false,
			isDraft: false,
			$or: [{
				email: regex
			}, {
				firstName: regex
			}, {
				lastName: regex
			}],
		};
	} else {
		searchStr = {
			isApprove: false,
			isDraft: false
		};
	}

	User.countDocuments({
		isApprove: false,
		isDraft: false
	}, function (err, c) {
		recordsTotal = c;
		User.countDocuments(searchStr, function (err, c) {
			recordsFiltered = c;
			User.find(
				searchStr,
				'_id firstName lastName email createdAt', {
					skip: Number(req.body.start),
					limit: Number(req.body.length),
					sort: '-createdAt',
				},
				function (err, results) {
					if (err) {
						console.log('error while getting results' + err);
						return;
					} else {
						var data = JSON.stringify({
							draw: req.body.draw,
							recordsFiltered: recordsFiltered,
							recordsTotal: recordsTotal,
							data: results,
						});
						return res.send(data);
					}
				}
			);
		});
	});
};

exports.getDraftApplication = async (req, res) => {
	User.find({
		isApprove: null,
		isDraft: true,
	}, function (err, users) {
		var users = JSON.stringify(users);
		res.render('admin/draft-application', {
			"draftUsers": users
		});
	});
};

exports.postDraftApplication = (req, res) => {
	var searchStr = req.body.search.value;
	if (req.body.search.value) {
		var regex = new RegExp(req.body.search.value, 'i');
		searchStr = {
			isApprove: null,
			isDraft: true,
			$or: [{
				email: regex
			}, {
				firstName: regex
			}, {
				lastName: regex
			}],
		};
	} else {
		searchStr = {
			isApprove: null,
			isDraft: true
		};
	}

	User.countDocuments({
		isApprove: null,
		isDraft: true
	}, function (err, c) {
		recordsTotal = c;
		User.countDocuments(searchStr, function (err, c) {
			recordsFiltered = c;
			User.find(
				searchStr,
				'_id firstName lastName email createdAt', {
					skip: Number(req.body.start),
					limit: Number(req.body.length),
					sort: '-createdAt',
				},
				function (err, results) {
					if (err) {
						console.log('error while getting results' + err);
						return;
					} else {
						var data = JSON.stringify({
							draw: req.body.draw,
							recordsFiltered: recordsFiltered,
							recordsTotal: recordsTotal,
							data: results,
						});
						return res.send(data);
					}
				}
			);
		});
	});
};