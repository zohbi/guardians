const AccountOpenignAPIHandler 			= require('../services/accountOpeningAPIService.js');
const { setCache, getCache }        = require('../../../lib/utilities/cache');
const { db } = require('../models/user/user');
const User 			                    = require('../models/user/user');
const configs                       = require('../../../config/index.js').config;
const moment                        = require("moment");

const Login = async () => {
    if(!getCache("accountOpeningAccessToken") || new moment(getCache("accountOpeningTokenTimeout"), 'MMDDyyyy HHmmss') <= new moment()){
      console.log("CALLING LOGIN");  
      const response = await AccountOpenignAPIHandler.loginRequest(configs.accountOpeningUserName, configs.accountOpeningSecret);
        var expires = new moment().add(response.data.expires_in - 60, 'seconds');
        setCache("accountOpeningAccessToken", response.data.access_token);
        setCache("accountOpeningRSAPublicKey", response.data.publicKey);
        setCache("accountOpeningTokenTimeout", expires.format("MMDDyyyy HHmmss"));
    }

    // setCache("accountOpeningAccessToken", '');
    // setCache("accountOpeningRSAPublicKey", '-----BEGIN PUBLIC KEY-----\r\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCobtNh/OJAuP1t9wnd2CEVfZ5x\r\nAG8G91dtC4bCLVrBjzdUhNsINTvF0C0vblijzUbCi+o++mFezlnuljcfLsS5adxy\r\nw/oX9nL89bOprC5mlgmn8sTEMc5ceF0HqO/iPwEVbQ/CLSYC1ucQYYVBqbNe8r4/\r\n2akAjplYZmpuB1gScQIDAQAB\r\n-----END PUBLIC KEY-----\r\n');
}

exports.callAccountOpenignAPI = async (req, res) => {
  try {
      await Login();
      //const response = await AccountOpenignAPIHandler.loginRequest(configs.accountOpeningUserName, configs.accountOpeningSecret);
      //setCache("accountOpeningAccessToken", response.data.access_token);
      //setCache("accountOpeningRSAPublicKey", response.data.publicKey);
  
      const user = await User.findById(req).lean();
      var res = await AccountOpenignAPIHandler.setupUser(user);
      await AccountOpenignAPIHandler.otherData(user, res.data);
      await User.updateOne({ _id: user._id }, { userIdentificationCode: res.data }, function(err, res){
          if(err){
              console.log(err);
          }
          else{
              console.log(res);
              console.log("UserIdentificationCodeSaved Successfully");
          }
      });
  } catch (err) {
    printErrorMessages('callAccountOpenignAPI', err);
  }
};

exports.iClearAccountOpening = async (req, res) => {
  try {
    const response = await AccountOpenignAPIHandler.loginRequest(configs.accountOpeningUserName, configs.accountOpeningSecret)
    setCache("accountOpeningAccessToken", response.data.access_token);
    setCache("accountOpeningRSAPublicKey", response.data.publicKey);

    const user = await User.findById(req).lean();
    var res = await AccountOpenignAPIHandler.openIClearAccount(user, 'M');
    res = await AccountOpenignAPIHandler.openIClearAccount(user, 'C');
    console.log("Successfully Added IClear Account");
  } catch (err) {
    printErrorMessages('iClearAccountOpening', err);
  }
}

exports.getBridgerResponseDetail = async (req, res) => {
  try{
    await Login();
    const user = await User.findOne({ _id: req.params.id }).lean();
  
    let userBridgerParams = {
      "userIdentifierCode": user.userIdentificationCode,
      "getOFACResponses": false
    };
    
    //res.render('admin/bridger-response', { data: sampleResponse });
  
    if (user.userIdentificationCode) {
      const bridgerResponse = await AccountOpenignAPIHandler.getBridgerResponse(userBridgerParams);
      res.render('admin/bridger-response', { data: bridgerResponse.data, user: user });
    }
    else {
      res.render('admin/bridger-response', { data: [], user: user });
    }
  }
  catch(err){
    printErrorMessages('getBridgerResponseDetail', err);
  }
}

exports.getTrueIdResponseDetail = async (req, res) => {
  try {
    const response1 = await AccountOpenignAPIHandler.loginRequest(configs.accountOpeningUserName, configs.accountOpeningSecret)
    setCache("accountOpeningAccessToken", response1.data.access_token);
    setCache("accountOpeningRSAPublicKey", response1.data.publicKey);

    const user = await User.findOne({ _id: req.params.id }).lean();
    //get userIdentifierCode from userDetail
    let userTrueIdParams = {
      "userIdentifierCode": user.userIdentificationCode
    };

    //res.render('admin/trueId-response', { data: sampleResponse });
    if (user.userIdentificationCode) {
      const trueIdResponse = await AccountOpenignAPIHandler.getTrueIdResponse(userTrueIdParams);
      res.render('admin/trueId-response', { data: trueIdResponse.data, user: user });
    }
    else {
      res.render('admin/trueId-response', { data: [], user: user });
    }
  }
  catch (err) {
    printErrorMessages('getTrueIdResponseDetail', err);
  }
}

const printErrorMessages = (key, error) => {
  // if (error.response.status == 401)
  //     console.log(key, "Please login first.");
  // else if (error.response.status == 400) {
  //     console.log(key, error.response.data);
  // }
  // else {
  //     console.log(key, error);
  // }
}