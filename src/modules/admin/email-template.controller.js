const Configuration = require('../models/admin/configuration');

exports.getEmailTemplateRejected = async (req, res) => {
  const configuration = await Configuration.findOne(
    {},
    'rejectedtemp.subject rejectedtemp.email'
  ).lean();
  res.render('admin/email-template-rejected', { configuration });
};

exports.postEmailTemplateRejected = async (req, res) => {
  try {
    const configuration = await Configuration.findByIdAndUpdate(
      req.body.id,
      {
        'rejectedtemp.subject': req.body.subject,
        'rejectedtemp.email': req.body.compose_email,
      },
      { upsert: true, new: true }
    );
    req.flash('success', 'Email Template Updated Successfully');
    res.render('admin/email-template-rejected', { configuration });
  } catch (err) {
    console.log(err);
  }
};

exports.getEmailTemplateAccepted = async (req, res) => {
  const configuration = await Configuration.findOne(
    {},
    'acceptedtemp.subject acceptedtemp.email'
  ).lean();
  res.render('admin/email-template-accepted', { configuration });
};

exports.postEmailTemplateAccepted = async (req, res) => {
  try {
    const configuration = await Configuration.findByIdAndUpdate(
      req.body.id,
      {
        'acceptedtemp.subject': req.body.subject,
        'acceptedtemp.email': req.body.compose_email,
      },
      { upsert: true, new: true }
    );
    req.flash('success', 'Email Template Updated Successfully');
    res.render('admin/email-template-accepted', { configuration });
  } catch (err) {
    console.log(err);
  }
};
