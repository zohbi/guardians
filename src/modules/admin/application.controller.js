const path = require('path');
const User = require('../models/user/user');
const RejectedUser = require('../models/user/rejectedUser');
const Audit = require('../models/common/audit');
const Admin = require('../models/admin/admin');
const 	moment 					= require('moment');
let pdf = require("html-pdf");
var wkhtmltopdf = require('wkhtmltopdf');
let ejs = require("ejs");
const {
    postSendEmail
} = require('../config/mail');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(
    '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'
);
const docusign = require('docusign-esign');
const fs = require('fs');
const nodemailer = require('nodemailer');
const Configuration = require('../models/admin/configuration');
const accOpeningController = require('./account.opening.api.controller');
let transport;
let hostEmail;
const configuration = Configuration.findOne({},
        'service host port hostemail hostpassword'
    )
    .then((config) => {
        if (config) {
            hostEmail = config.hostemail;
            transport = nodemailer.createTransport({
                host: config.host,
                port: config.port,
                auth: {
                    user: config.hostemail,
                    pass: config.hostpassword
                }
            });
        } else {
            hostEmail = 'planet01.testing@gmail.com';
            transport = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                auth: {
                    user: 'planet01.testing@gmail.com',
                    pass: 'Joker182'
                }
            });
        }
    })
    .catch((err) => console.log(err));
exports.getApplication = async(req, res) => {
    try {
        let user = {};
        if (req.params.status == "rejected") {
            user = await RejectedUser.findOne({
                _id: req.params.id
            }).lean();
            user = user.applicationDetail
        } else {
            user = await User.findOne({
                _id: req.params.id
            }).lean();
        }

        const audit = await Audit.findOne({
                userId: user._id
            })
            .populate({
                path: 'applicationStatus.admin'
            })
            .populate({
                path: 'draftEmail',
                populate: {
                    path: 'emailSend',
                },
            })
            .lean();

        res.render('admin/application-detail', {
            user,
            back: req.params.status,
            audit,
        });
    } catch (err) {
        console.log(err);
    }
};

exports.getApplicationPDF = async(req, res) => {
    try {
        let user = {};
        user = await User.findOne({
            _id: req.params.id
        }).lean();
		let temp_admin = {};
		temp_admin = await Audit.findOne({
            userId: req.params.id
        }).lean();

		let admin = {};
		if(temp_admin.applicationStatus.type == 'Application accepted')
		{
			admin = await Admin.findOne({
				_id: temp_admin.applicationStatus.admin
			}).lean();
		}

		let accepted_date;
		accepted_date = temp_admin.applicationStatus.date;

        // res.render('admin/pdf_template', {
        //     user: user,
		// 	admin: admin,
        //     dob:moment(user.dateOfBirth).format('MMM-DD-YYYY'),
        //     issue_date:moment(user.issueDate).format('MMM-DD-YYYY'),
        //     expire_date:moment(user.expDate).format('MMM-DD-YYYY'),
		// 	create_date: moment(user.createdAt).format('MMM-DD-YYYY'),
		// 	accepted_date: moment(accepted_date).format('MMM-DD-YYYY'),
        //     image_logo: path.join('file:///', __dirname, "../public/assets/img/GUARDIANTRADINGblack_FINRASIPC.png"),
        //     image_logo_2: path.join('file:///', __dirname, "../public/assets/img/GUARDIANTRADINGblack_FINRASIPC.png"),
        //     signature: path.join('file:///', __dirname, "../public/" + user.signatureName),
        //     adminSignature: path.join('file:///', __dirname, "../public/" + user.adminSignature),
        //     app_url: path.join('file:///'+ __dirname ,"../public"),
        // });

        ejs.renderFile(path.join(__dirname, '../../../src/modules/views/admin/pdf_template.ejs'), {
            user: user,
			admin: admin,
            trusteddob:moment(user.trusteddob).format('MMM-DD-YYYY'),
            dob:moment(user.dateOfBirth).format('MMM-DD-YYYY'),
            issue_date:moment(user.issueDate).format('MMM-DD-YYYY'),
            expire_date:moment(user.expDate).format('MMM-DD-YYYY'),
			create_date: moment(user.createdAt).format('MMM-DD-YYYY'),
			accepted_date: moment(accepted_date).format('MMM-DD-YYYY'),
            image_logo: path.join('file:///', __dirname, "../public/assets/img/GUARDIANTRADINGblack_FINRASIPC.png"),
            image_logo_2: path.join('file:///', __dirname, "../public/assets/img/GUARDIANTRADINGblack_FINRASIPC.png"),
            signature: path.join('file:///', __dirname, "../public/" + user.signatureName),
            adminSignature: path.join('file:///', __dirname, "../public/" + user.adminSignature),
            app_url: path.join('file:///'+ __dirname ,"../public"),
        }, (err, data) => {
            // if (err) {          
            // 	res.send(err);    
            // } 
            // else {        
            // 	wkhtmltopdf(data, { pageSize: 'letter' }).pipe(fs.createWriteStream('out.pdf'));
            // 	// let file = path.join(__dirname,'../../../out.pdf');
            // 	// res.sendFile(file);
            // 	res.send('');
            // }
            let options = {
                "height": "11.25in",
                "width": "8.5in",
                "header": {
                    "height": "20mm"
                },
                "footer": {
                    "height": "20mm",
                },
            };
            let cur_date = new Date();
            let formattedDate = String(cur_date.getMonth()).padStart(2, '0')+"_"+String(cur_date.getDate()).padStart(2, '0')+"_"+cur_date.getFullYear();
            var file_name = user.firstName + '_' + user.lastName + '_guardian_account_application_' + formattedDate + ".pdf";
            console.log(path.join(__dirname, "../public/users/pdf/", file_name));
            pdf.create(data, options).toFile(path.join(__dirname, "../public/users/pdf/", file_name), function(err, data) {
                if (err) {
                    res.send(err);
                    console.log(err);
                } else {
                    res.redirect('/admin/user-file/' + file_name);
                    //res.download(file,user.firstName+".pdf");
                    // res.send("File created successfully");
                }
            });
        });
        // console.log(html);
        // wkhtmltopdf('http://google.com', { output: 'out.pdf' }); 




    } catch (err) {
        console.log(err);
    }
};

exports.getPdfFile = async(req, res) => {
    let file = path.join(__dirname, "../public/users/pdf/", req.params.file);
    res.sendFile(file);
    //    res.send( req.params.file);
}

exports.acceptApplication = async(req, res) => {

    /*
    	try {
    		req.session.userId 				= req.params.id;
    		req.session.applicationSubject 	= req.body.applicationSubject;
    		req.session.applicationEmail 	= req.body.applicationEmail;
    		req.session.applicationReason 	= req.body.applicationReason;
    		req.session.pdfAdmin 			= 'ws' + req.params.id + '.pdf';
    		res.redirect('/admin/ds/login');
    	} catch (err) {
    		console.log(err);
    	}
    */

    try {
        const user = await User.findByIdAndUpdate(
            req.params.id, {
                isApprove: true,
                adminSignature: req.body.adminSignature
            }, {
                new: true
            }
        );

        const subject = req.body.applicationSubject;
        const text = req.body.applicationEmail.replace('@name', `${user.firstName} ${user.lastName}`);
        const message = {
            from: hostEmail,
            to: user.email,
            subject: subject,
            html: text
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                console.log(err)
            } else {
                console.log(info);
            }
        });

        let userAudit = await Audit.updateOne({
            userId: req.params.id
        }, {
            'applicationStatus.type': 'Application accepted',
            'applicationStatus.admin': res.locals.admin._id,
            'applicationStatus.message': text,
            'applicationStatus.reason': req.body.applicationReason,
            'applicationStatus.date': Date.now(),
        }, {
            new: true
        });

        console.log("APPLICAION ACCEPTED");

        accOpeningController.iClearAccountOpening(req.params.id);

        res.redirect(req.header('Referer') || '/admin/dashboard');
    } catch (err) {
        console.log(err);
    }
};

// exports.acceptApplication = async (req, res) => {
//   try {
//     const user = await User.findByIdAndUpdate(
//       req.params.id,
//       {
//         isApprove: true,
//       },
//       { new: true }
//     );

//     const subject = req.body.applicationSubject;
//     const text = req.body.applicationEmail.replace(
//       '@name',
//       `${user.firstName} ${user.lastName}`
//     );

//     postSendEmail(user.email, subject, text);

//     let userAudit = await Audit.updateOne(
//       { userId: req.params.id },
//       {
//         'applicationStatus.type': 'Application accepted',
//         'applicationStatus.admin': res.locals.admin._id,
//         'applicationStatus.message': text,
//         'applicationStatus.reason': req.body.applicationReason,
//         'applicationStatus.date': Date.now(),
//       },
//       { new: true }
//     );
//     return res.redirect('/admin/dashboard');
//   } catch (err) {
//     console.log(err);
//   }
// };

exports.rejectApplication = async(req, res) => {
    try {
        const user = await User.findByIdAndUpdate(
            req.params.id, {
                isApprove: false,
            }, {
                new: true
            }
        );
        let rejectedUser = new RejectedUser({
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            reason: req.body.applicationReason,
            applicationDetail: user
        });
        rejectedUser = await rejectedUser.save();
        await User.findByIdAndRemove(req.params.id)

        const subject = req.body.applicationSubject;
        const text = req.body.applicationEmail.replace(
            '@name',
            `${user.firstName} ${user.lastName}`
        );

        // sgMail
        // 	.send({
        // 		to: user.email,
        // 		from: process.env.SENDGRIDEMAIL,
        // 		subject: subject,
        // 		html: text,
        // 	})
        // 	.then(() => {
        // 		console.log('Email sent');
        // 	})
        // 	.catch((error) => {
        // 		console.error(error.response.body);
        // 	});

        const message = {
            from: hostEmail,
            to: user.email,
            subject: subject,
            html: text
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                console.log(err)
            } else {
                console.log(info);
            }
        });

        // postSendEmail(user.email, subject, text);

        let userAudit = await Audit.updateOne({
            userId: req.params.id
        }, {
            'rejectUserId': rejectedUser._id,
            'applicationStatus.type': 'Application rejected',
            'applicationStatus.admin': res.locals.admin._id,
            'applicationStatus.message': text,
            'applicationStatus.reason': req.body.applicationReason,
            'applicationStatus.date': Date.now(),
        }, {
            new: true
        });
        return res.redirect('/admin/dashboard');
    } catch (err) {
        console.log(err);
    }
};

exports.draftEmail = async(req, res) => {
    try {
        const user = await User.findOne({
            _id: req.params.id
        }).lean();

        const subject = req.body.applicationSubject;
        const text = req.body.applicationEmail;

        // sgMail
        // 	.send({
        // 		to: user.email,
        // 		from: process.env.SENDGRIDEMAIL,
        // 		subject: subject,
        // 		html: text,
        // 	})
        // 	.then(() => {
        // 		console.log('Email sent');
        // 	})
        // 	.catch((error) => {
        // 		console.error(error.response.body);
        // 	});

        const message = {
            from: hostEmail,
            to: user.email,
            subject: subject,
            html: text
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                console.log(err)
            } else {
                console.log(info);
            }
        });

        //    postSendEmail(user.email, subject, text);

        let userAudit = await Audit.updateOne({
            userId: req.params.id
        }, {
            $push: {
                draftEmail: {
                    type: 'Draft Email',
                    emailSend: res.locals.admin._id,
                    message: text,
                    date: Date.now(),
                },
            },
        }, {
            new: true
        });

        return res.redirect('/admin/dashboard');
    } catch (err) {
        console.log(err);
    }
};

exports.applicationStatusChange = async(req, res) => {
    try {
        const user = await User.findOne({
            _id: req.params.id
        }).lean();

        const subject = req.body.applicationSubject;
        const text = req.body.applicationEmail;

        if (user) {
            let applicaton = await User.findByIdAndUpdate(
                req.params.id, {
                    isApprove: null,
                    isDraft: true,
                }, {
                    new: true
                }
            );

            // sgMail
            // 	.send({
            // 		to: user.email,
            // 		from: process.env.SENDGRIDEMAIL,
            // 		subject: subject,
            // 		html: text,
            // 	})
            // 	.then(() => {
            // 		console.log('Email sent');
            // 	})
            // 	.catch((error) => {
            // 		console.error(error.response.body);
            // 	});

            const message = {
                from: hostEmail,
                to: user.email,
                subject: subject,
                html: text
            };
            transport.sendMail(message, function(err, info) {
                if (err) {
                    console.log(err)
                } else {
                    console.log(info);
                }
            });

            // postSendEmail(user.email, subject, text);
        }

        return res.redirect('/admin/dashboard');
    } catch (err) {
        console.log(err);
    }
};

exports.acceptSignRedirect = async(req, res) => {
    try {
        const user = await User.findByIdAndUpdate(
            req.session.userId, {
                isApprove: true,
            }, {
                new: true
            }
        );
        /*
        	let dsApiClient = new docusign.ApiClient();
        	dsApiClient.setBasePath(req.session.basePath);
        	dsApiClient.addDefaultHeader(
        		'Authorization',
        		'Bearer ' + req.user.accessToken
        	);
        	let envelopesApi = new docusign.EnvelopesApi(dsApiClient),
        		results = null;

        	// Step 1. EnvelopeDocuments::get.
        	// Exceptions will be caught by the calling function
        	results = await envelopesApi.getDocument(
        		req.session.accountId,
        		req.query.envelopeId,
        		'3',
        		null
        	);
        */

        const subject = req.session.applicationSubject;
        const text = req.session.applicationEmail.replace('@name', `${user.firstName} ${user.lastName}`);
        const message = {
            from: hostEmail,
            to: user.email,
            subject: subject,
            html: text
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                console.log(err)
            } else {
                console.log(info);
            }
        });
        /*
        	var filename = req.session.userId + '.pdf';

        	fs.writeFile(
        		'/src/modules/public/users/pdf/' + filename,
        		results,
        		'binary',
        		function (err, data) {
        			if (err) return console.log(err);
        			// sgMail
        			// 	.send({
        			// 		to: user.email,
        			// 		from: process.env.SENDGRIDEMAIL,
        			// 		subject: subject,
        			// 		html: text,
        			// 		// files: [{filename:`${user._id}.pdf`}],
        			// 	})
        			// 	.then(() => {
        			// 		console.log('Email sent');
        			// 	})
        			// 	.catch((error) => {
        			// 		console.error(error);
        			// 	});
        			
        			
        		}
        	);

        	postSendEmail(user.email, subject, text, `${user._id}.pdf`);
        */

        let userAudit = await Audit.updateOne({
            userId: req.params.id
        }, {
            'applicationStatus.type': 'Application accepted',
            'applicationStatus.admin': res.locals.admin._id,
            'applicationStatus.message': text,
            'applicationStatus.reason': req.session.applicationReason,
            'applicationStatus.date': Date.now(),
        }, {
            new: true
        });
        return res.redirect('/admin/dashboard');
    } catch (err) {
        console.log(err);
    }
};