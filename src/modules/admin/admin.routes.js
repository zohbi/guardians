const express 	= require('express');
const Admin		= require('../models/admin/admin');
const router 	= express.Router();
const {
	getLogin,
	postLogin,
	getDasboard,
	logout,
	adminSignatureSave
} = require('./auth.controller');
const {
	getApplication,
	getApplicationPDF,
	acceptApplication,
	rejectApplication,
	draftEmail,
	applicationStatusChange,
	acceptSignRedirect,
	getPdfFile
} = require('./application.controller');
const {
	getPendingApplication,
	postPendingApplication,
	getAcceptedApplication,
	postAcceptedApplication,
	getRejectedApplication,
	postRejectedApplication,
	getDraftApplication,
	postDraftApplication,
} = require('./datatable.controller');
const {
	getAudit
} = require('./audit.controller');
const {
	getEmailCredentials,
	postEmailCredentials,
} = require('./configuration.controller');
const {
	getEmailTemplateRejected,
	postEmailTemplateRejected,
	getEmailTemplateAccepted,
	postEmailTemplateAccepted,
} = require('./email-template.controller');
const {
	getAdmin,
	getDatatables,
	getAddUser,
	postAddUser,
	getEditUser,
	postEditUser,
	postDeleteUser,
} = require('./user-management.controller');
const {
	getForgotPassword,
	postForgotPassword,
	getError,
	getNewPassword,
	postNewPassword,
  } = require('./forgot.controller');

const {
	ensureAuthenticated,
	forwardAuthenticated,
} = require('../middleware/admin.auth');

const {
	getBridgerResponseDetail,
	getTrueIdResponseDetail
} = require('./account.opening.api.controller');

router.get('/forgot-password', getForgotPassword);
router.post('/forgot-password', postForgotPassword);
router.get('/error', getError);
router.get('/new-password/:id/:token', getNewPassword);
router.post('/new-password/:id', postNewPassword);

router.get('/login', forwardAuthenticated, getLogin);
router.post('/login', forwardAuthenticated, postLogin);
router.get('/dashboard', ensureAuthenticated, getDasboard);
router.get('/bridger-response/:id', ensureAuthenticated, getBridgerResponseDetail);
router.get('/trueId-response/:id', ensureAuthenticated, getTrueIdResponseDetail);
router.get(
	'/application-details/:status/:id',
	ensureAuthenticated,
	getApplication
);
router.get('/user-pdf/:id', ensureAuthenticated, getApplicationPDF);
router.get('/user-file/:file', ensureAuthenticated, getPdfFile);

router.post('/application-accept/:id', ensureAuthenticated, acceptApplication);
router.post('/application-reject/:id', ensureAuthenticated, rejectApplication);
router.post('/application-draftemail/:id', ensureAuthenticated, draftEmail);
router.post(
	'/application-statuschange/:id',
	ensureAuthenticated,
	applicationStatusChange
);
router.get('/logout', logout);

router.get('/pending-application', ensureAuthenticated, getPendingApplication);
router.post(
	'/pending-application',
	ensureAuthenticated,
	postPendingApplication
);
router.post('/adminSignatureSave', ensureAuthenticated, adminSignatureSave);

router.get(
	'/accepted-application',
	ensureAuthenticated,
	getAcceptedApplication
);
router.post(
	'/accepted-application',
	ensureAuthenticated,
	postAcceptedApplication
);

router.get(
	'/rejected-application',
	ensureAuthenticated,
	getRejectedApplication
);
router.post(
	'/rejected-application',
	ensureAuthenticated,
	postRejectedApplication
);

router.get('/draft-application', ensureAuthenticated, getDraftApplication);
router.post('/draft-application', ensureAuthenticated, postDraftApplication);

router.get('/email-credentials', ensureAuthenticated, getEmailCredentials);
router.post('/email-credentials', ensureAuthenticated, postEmailCredentials);

router.get('/audit', ensureAuthenticated, getAudit);
router.get('/user-management', ensureAuthenticated, getAdmin);
router.post('/user-management/datatables', ensureAuthenticated, getDatatables);
router.get('/user-management/add-user', ensureAuthenticated, getAddUser);
router.post('/user-management/add-user', ensureAuthenticated, postAddUser);
router.get('/user-management/edit-user/:id', ensureAuthenticated, getEditUser);

router.post(
	'/user-management/edit-user/:id',
	ensureAuthenticated,
	postEditUser
);
router.post(
	'/user-management/delete-user/:id',
	ensureAuthenticated,
	postDeleteUser
);

router.get(
	'/email-template-rejected',
	ensureAuthenticated,
	getEmailTemplateRejected
);
router.post(
	'/email-template-rejected',
	ensureAuthenticated,
	postEmailTemplateRejected
);
router.get(
	'/email-template-accepted',
	ensureAuthenticated,
	getEmailTemplateAccepted
);
router.post(
	'/email-template-accepted',
	ensureAuthenticated,
	postEmailTemplateAccepted
);

router.post('/application-accept/status', ensureAuthenticated, acceptSignRedirect);
module.exports = router;