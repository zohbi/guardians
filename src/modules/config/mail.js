const Configuration = require('../models/admin/configuration');
const nodemailer = require('nodemailer');
require('dotenv').config();

var transporter;

const configuration = Configuration.findOne(
  {},
  'service host port hostemail hostpassword'
)
  .then((config) => {
    if (config) {
      transporter = nodemailer.createTransport({
        service: config.service,
        host: config.host,
        port: config.port,
        secure: true,
        auth: {
          user: config.hostemail,
          pass: config.hostpassword,
        },
      });
    } else {
      transporter = nodemailer.createTransport({
        service: process.env.SERVICE,
        host: process.env.HOSTEMAIL,
        port: 587,
        secure: true,
        auth: {
          user: process.env.MAILEMAIL,
          pass: process.env.MAILPASSWORD,
        },
      });
    }
  })
  .catch((err) => console.log(err));

const sendMail = async (mailOptions, cb) => {
  transporter.sendMail(mailOptions, cb);
};
let postSendEmail = (email, subject, text, attachment = null) => {
  let mailOptions;
  if (attachment) {
    mailOptions = {
      from: configuration.hostemail || process.env.MAILEMAIL,
      to: email,
      subject: subject,
      html: text,
      attachments: [
        {
          filename: attachment,
          path: 'src/modules/public/users/pdf/' + attachment,
          cid: 'report.pdf',
        },
      ],
    };
  } else {
    mailOptions = {
      from: configuration.hostemail || process.env.MAILEMAIL,
      to: email,
      subject: subject,
      html: text,
    };
  }

  let mailSent = sendMail(mailOptions, function (error, data) {
    if (error) {
      console.log(error);
    }
  });
};

module.exports = { postSendEmail };
