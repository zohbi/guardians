/**
 * @file
 * This file provides common controllers.
 * @author DocuSign
 */

const fs = require('fs'),
  dsConfig = require('../config/appsettings.json'),
  User = require('../src/modules/models/user/user'),
  docusign = require('docusign-esign'),
  documentationTopic = 'auth-code-grant-node';
const commonControllers = exports;

const path = require('path');

/**
 * Home page for this application
 */
commonControllers.indexController = (req, res) => {
  if (dsConfig.quickstart == 'true' && req.user == undefined) {
    console.debug('quickstart mode on');
    return res.redirect('/admin/docusign');
  } else {
    res.render('pages/index', {
      title: 'Home',
      documentation: dsConfig.documentation + documentationTopic,
      showDoc: dsConfig.documentation,
    });
  }
};

commonControllers.mustAuthenticateController = (req, res) => {
  if (dsConfig.quickstart == 'true') res.redirect('/admin/login');
  else return res.redirect('/admin/ds/login');
  // res.render('pages/ds_must_authenticate', {
  //   title: 'Authenticate with DocuSign',
  // });
};

commonControllers.login = (req, res, next) => {
  // const { auth } = req.query;
  // if (auth === 'grand-auth') {
  //   req.dsAuth = req.dsAuthCodeGrant;
  // } else if (auth === 'jwt-auth') {
  //   req.dsAuth = req.dsAuthJwt;
  // }
  req.dsAuth = req.dsAuthJwt;

  req.dsAuth.login(req, res, next);
};

commonControllers.logout = (req, res) => {
  dsConfig.quickstart = 'false';
  req.dsAuth.logout(req, res);
};

commonControllers.logoutCallback = (req, res) => {
  req.dsAuth.logoutCallback(req, res);
};

/**
 * Display parameters after DS redirect to the application
 * after an embedded signing ceremony, etc
 * @param {object} req Request object
 * @param {object} res Result object
 */
