const JSEncrypt = require("node-jsencrypt");
const { getCache } = require('./cache');
const fs = require('fs');
const FormData = require('form-data');
const jsEncrypt = new JSEncrypt();

const encryptJsonData = (body) => {
    jsEncrypt.setPublicKey(getCache("accountOpeningRSAPublicKey"));
    for (let key in body) {
        if (Array.isArray(body[key])) {
            body[key] = jsEncrypt.encrypt(JSON.stringify(body[key]));
        } else {
            body[key] = jsEncrypt.encrypt(body[key].toString());
        }
    }
    return body;
}

const encryptFormData = (body) => {
    jsEncrypt.setPublicKey(getCache("accountOpeningRSAPublicKey"));
    var data = new FormData();
    for (let key in body) {
        if (/\\/.test(body[key]))
        {
            try{
                console.log(key);
                console.log(body[key]);
                //var stream = fs.createReadStream(body[key]);
                data.append(key, fs.createReadStream(body[key]));
            }
            catch(error){
                console.log(error);
            }
        }
        else {
            data.append(key, jsEncrypt.encrypt(body[key].toString()).toString());
        }
    }
    return data;
}

module.exports = { encryptJsonData, encryptFormData };